syntheticsun = {}

local range_small = 20
local range_big = 50
local areas = AreaStore('suns')
syntheticsun.suns = {}
syntheticsun.count = 1

minetest.register_craft({
    output = 'syntheticsun:whitedwarf 1',
    recipe = {
        {'default:obsidian_glass', 'default:mese', 'default:obsidian_glass'},
        {'homedecor:power_crystal', 'bucket:bucket_water', 'homedecor:power_crystal'},
        {'default:obsidian_glass', 'homedecor:power_crystal', 'default:obsidian_glass'},
    }
})

minetest.register_craft({
    output = 'syntheticsun:redgiant 1',
    recipe = {
        {'', 'syntheticsun:whitedwarf', ''},
        {'syntheticsun:whitedwarf', 'bucket:bucket_lava', 'syntheticsun:whitedwarf'},
        {'', 'syntheticsun:whitedwarf', ''},
    }
})

minetest.register_node("syntheticsun:corona", {
    tiles = { "corona.png" },
    drawtype = "airlike",
    walkable = false,
    pointable = false,
    diggable = false,
    climbable = false,
    buildable_to = true,
    sunlight_propagates = true,
    paramtype = "light",
    light_source = 14,
    drop = "",
    diggable= false,
    air_equivalent = true,
    floodable = true,
    paramtype = "light",
    groups = {sun = 1},
})

minetest.register_node("syntheticsun:corona_mid", {
    tiles = { "corona.png" },
    drawtype = "airlike",
    walkable = false,
    pointable = false,
    diggable = false,
    climbable = false,
    buildable_to = true,
    sunlight_propagates = true,
    paramtype = "light",
    light_source = 10,
    drop = "",
    diggable = false,
    air_equivalent = true,
    floodable = true,
    paramtype = "light",
    groups = {sun = 1},
})

minetest.register_node("syntheticsun:corona_low", {
    tiles = { "corona.png" },
    drawtype = "airlike",
    walkable = false,
    pointable = false,
    diggable = false,
    climbable = false,
    buildable_to = true,
    sunlight_propagates = true,
    paramtype = "light",
    light_source = 7,
    drop = "",
    diggable = false,
    air_equivalent = true,
    floodable = true,
    paramtype = "light",
    groups = {sun = 1},
})

minetest.register_node("syntheticsun:corona_damage", {
    tiles = { "corona.png" },
    drawtype = "airlike",
    walkable = false,
    pointable = false,
    diggable = false,
    climbable = false,
    buildable_to = true,
    sunlight_propagates = true,
    paramtype = "light",
    light_source = minetest.LIGHT_MAX,
    damage_per_second = 2,
    drop = "",
    diggable = false,
    air_equivalent = true,
    floodable = true,
    paramtype = "light",
    groups = {sun = 1, igniter = 2},
})

minetest.register_node("syntheticsun:corona_damage_heavy", {
    tiles = { "corona.png" },
    drawtype = "airlike",
    walkable = false,
    pointable = false,
    diggable = false,
    climbable = false,
    buildable_to = true,
    sunlight_propagates = true,
    paramtype = "light",
    light_source = minetest.LIGHT_MAX,
    damage_per_second = 4,
    drop = "",
    diggable = false,
    air_equivalent = true,
    floodable = true,
    paramtype = "light",
    groups = {sun = 1, igniter = 2},
})

minetest.register_node("syntheticsun:whitedwarf", {
    description = "Small, sun like white dwarf star emitting some light",
    inventory_image = minetest.inventorycube("whitedwarf.png", "whitedwarf.png", "whitedwarf.png"),
    tiles = { "whitedwarf.png" },
    drawtype = "glasslike",
    groups = { snappy=3, oddly_breakable_by_hand=3, igniter=2 },
    drop = "syntheticsun:whitedwarf",
    light_source = 14,
    paramtype = "light",
    on_construct = function(pos)
        local dist = range_small
        local minp = { x=pos.x-dist, y=pos.y-dist, z=pos.z-dist }
        local maxp = { x=pos.x+dist, y=pos.y+dist, z=pos.z+dist }

        syntheticsun.construct_whitedwarf(minp, maxp, pos)

        local aId = areas:insert_area(minp, maxp, 'sun_whitedwarf_'..syntheticsun.count)
        syntheticsun.suns[syntheticsun.count] = {position=pos, edge1=minp, edge2=maxp, sun='sun_whitedwarf_'..syntheticsun.count, aid=aId}
        syntheticsun.count = syntheticsun.count + 1
        syntheticsun:save_data()
    end,
    on_destruct = function(pos)
        local dist = range_small
        local minp = { x=pos.x-dist, y=pos.y-dist, z=pos.z-dist }
        local maxp = { x=pos.x+dist, y=pos.y+dist, z=pos.z+dist }

        local manip = minetest.get_voxel_manip()
        local emin, emax = manip:read_from_map(minp, maxp)
        local area = VoxelArea:new({MinEdge=emin, MaxEdge=emax})
        local data = manip:get_data()

        local c_air = minetest.get_content_id("air")

        local c_sun = minetest.get_content_id("syntheticsun:corona")
        local glow_nodes = minetest.find_nodes_in_area(minp, maxp, "syntheticsun:corona")
        for i, npos in ipairs(glow_nodes) do
            local vi = area:indexp(npos)
            data[vi] = c_air
        end

        local c_sun_damage = minetest.get_content_id("syntheticsun:corona_damage")
        local glow_damage_nodes = minetest.find_nodes_in_area(minp, maxp, "syntheticsun:corona_damage")
        for i, npos in ipairs(glow_damage_nodes) do
            local vi = area:indexp(npos)
            data[vi] = c_air
        end

        local c_sun_mid = minetest.get_content_id("syntheticsun:corona_mid")
        local glow_mid_nodes = minetest.find_nodes_in_area(minp, maxp, "syntheticsun:corona_mid")
        for i, npos in ipairs(glow_mid_nodes) do
            local vi = area:indexp(npos)
            data[vi] = c_air
        end

        local c_sun_low = minetest.get_content_id("syntheticsun:corona_low")
        local glow_low_nodes = minetest.find_nodes_in_area(minp, maxp, "syntheticsun:corona_low")
        for i, npos in ipairs(glow_low_nodes) do
            local vi = area:indexp(npos)
            data[vi] = c_air
        end

        for i, sun in pairs(syntheticsun.suns) do
            if sun['position']['x'] == pos.x and
                sun['position']['y'] == pos.y and
                sun['position']['z'] == pos.z then
                areas:remove_area(sun['aid'])
                syntheticsun.suns[i] = nil
            end
        end
        syntheticsun.save_data()
        manip:set_data(data)
        manip:write_to_map()
        manip:update_map()
        local as = areas:get_areas_in_area(
            {x=pos.x-range_small, y=pos.y-range_small, z=pos.z-range_small},
            {x=pos.x+range_small, y=pos.y+range_small, z=pos.z+range_small},
            true, true, true)
        for k, a in pairs(as) do
            local sunpos = {
                x=math.abs(a.min.x - a.max.x)/2 + a.min.x,
                y=math.abs(a.min.y - a.max.y)/2 + a.min.y,
                z=math.abs(a.min.z - a.max.z)/2 + a.min.z}
            if string.find(a.data, "redgiant") then
                syntheticsun.construct_redgiant(a.min, a.max, sunpos)
            elseif string.find(a.data, "whitedwarf") then
                syntheticsun.construct_whitedwarf(a.min, a.max, sunpos)
            end
        end
    end,
})

minetest.register_node("syntheticsun:redgiant", {
    description = "Huge, sun like star emitting light",
    inventory_image = minetest.inventorycube("redgiant.png", "redgiant.png", "redgiant.png"),
    tiles = {{name="redgiant2.png", animation={type="vertical_frames", aspect_w=16, aspect_h=16, length=7.0}}},
    groups = { snappy=3, oddly_breakable_by_hand=3, igniter=2 },
    drop = "syntheticsun:redgiant",
    light_source = 14,
    paramtype = "light",
    on_construct = function(pos)
        local dist = range_big
        local minp = { x=pos.x-dist, y=pos.y-dist, z=pos.z-dist }
        local maxp = { x=pos.x+dist, y=pos.y+dist, z=pos.z+dist }

        syntheticsun.construct_redgiant(minp, maxp, pos)

        local aId = areas:insert_area(minp, maxp, "sun_redgiant_"..syntheticsun.count)
        syntheticsun.suns[syntheticsun.count] = {position=pos, edge1=minp, edge2=maxp, sun='sun_redgiant_'..syntheticsun.count, aid=aId}
        syntheticsun.count = syntheticsun.count + 1
        syntheticsun:save_data()
    end,
    on_destruct = function(pos)
        local dist = range_big
        local minp = { x=pos.x-dist, y=pos.y-dist, z=pos.z-dist }
        local maxp = { x=pos.x+dist, y=pos.y+dist, z=pos.z+dist }

        local manip = minetest.get_voxel_manip()
        local emin, emax = manip:read_from_map(minp, maxp)
        local area = VoxelArea:new({MinEdge=emin, MaxEdge=emax})
        local data = manip:get_data()

        local c_air = minetest.get_content_id("air")

        local c_sun = minetest.get_content_id("syntheticsun:corona")
        local glow_nodes = minetest.find_nodes_in_area(minp, maxp, "syntheticsun:corona")
        for i, npos in ipairs(glow_nodes) do
            local vi = area:indexp(npos)
            data[vi] = c_air
        end

        local c_sun_damage = minetest.get_content_id("syntheticsun:corona_damage_heavy")
        local glow_damage_nodes = minetest.find_nodes_in_area(minp, maxp, "syntheticsun:corona_damage_heavy")
        for i, npos in ipairs(glow_damage_nodes) do
            local vi = area:indexp(npos)
            data[vi] = c_air
        end

        local c_sun_mid = minetest.get_content_id("syntheticsun:corona_mid")
        local glow_mid_nodes = minetest.find_nodes_in_area(minp, maxp, "syntheticsun:corona_mid")
        for i, npos in ipairs(glow_mid_nodes) do
            local vi = area:indexp(npos)
            data[vi] = c_air
        end

        local c_sun_low = minetest.get_content_id("syntheticsun:corona_low")
        local glow_low_nodes = minetest.find_nodes_in_area(minp, maxp, "syntheticsun:corona_low")
        for i, npos in ipairs(glow_low_nodes) do
            local vi = area:indexp(npos)
            data[vi] = c_air
        end

        for i, sun in pairs(syntheticsun.suns) do
            if sun['position']['x'] == pos.x and
                sun['position']['y'] == pos.y and
                sun['position']['z'] == pos.z then
                areas:remove_area(sun['aid'])
                syntheticsun.suns[i] = nil
            end
        end
        syntheticsun.save_data()

        manip:set_data(data)
        manip:write_to_map()
        manip:update_map()

        local as = areas:get_areas_in_area(
            {x=pos.x-range_small, y=pos.y-range_small, z=pos.z-range_small},
            {x=pos.x+range_small, y=pos.y+range_small, z=pos.z+range_small},
            true, true, true)
        for k, a in pairs(as) do
            local sunpos = {
                x=math.abs(a.min.x - a.max.x)/2 + a.min.x,
                y=math.abs(a.min.y - a.max.y)/2 + a.min.y,
                z=math.abs(a.min.z - a.max.z)/2 + a.min.z}
            if string.find(a.data, "redgiant") then
                syntheticsun.construct_redgiant(a.min, a.max, sunpos)
            elseif string.find(a.data, "whitedwarf") then
                syntheticsun.construct_whitedwarf(a.min, a.max, sunpos)
            end

        end

    end,
})

--minetest.register_abm({
--    nodenames = {"syntheticsun:corona", "syntheticsun:corona_mid", "syntheticsun:corona_damage", "syntheticsun:corona_damage_heavy"},
--    neighbors = {"air"},
--    interval = 1.0,
--    chance = 1,
--    action = function(pos, node, active_objcet_count, active_object_count_wider)
--        minetest.log('action', 'triggered abm for air')
--        local water = minetest.find_node_near(pos, 2, "group:water")
--        local lava = minetest.find_node_near(pos, 2, "group:lava")
--        if water ~=nil or lava ~= nil then
--            return
--        end
--        local airnode = minetest.find_node_near(pos, 1, "air")
--        minetest.set_node(airnode, {name = node.name})
--    end
--})

--minetest.register_on_placenode(function(pos, newnode, placer, oldnode, itemstack, pointed_thing)
--    minetest.log('action', 'placed node and search for a sun'..oldnode.name)
--    if oldnode.name == 'syntheticsun:corona' then
--        local rpos = {x = pos.x, y = pos.y, z = pos.z}
--        local found_areas = areas:get_areas_for_pos(rpos, true, true)
--
--        minetest.log('action', 'area: '..dump(found_areas))
--        if next(found_areas) then
--            minetest.log('action', 'sun found!')
--        end
--    end
--end)

minetest.register_on_dignode(function(pos, oldnode, digger)
    local found_areas = areas:get_areas_for_pos({x=pos.x, y=pos.y, z=pos.z}, true, true)
    for k, area in pairs(found_areas) do
        syntheticsun.reconstruct(pos, area)
    end
end)

syntheticsun.construct_whitedwarf = function(min, max, pos)
    local manip = minetest.get_voxel_manip()
    local minp = {x=min.x, y=min.y, z=min.z}
    local maxp = {x=max.x, y=max.y, z=max.z}
    local emin, emax = manip:read_from_map(minp, maxp)
    local area = VoxelArea:new({MinEdge=emin, MaxEdge=emax})
    local data = manip:get_data()
    local c_air = minetest.get_content_id("air")
    local c_sun = minetest.get_content_id("syntheticsun:corona")
    local c_sun_mid = minetest.get_content_id("syntheticsun:corona_mid")
    local c_sun_low = minetest.get_content_id("syntheticsun:corona_low")
    local c_sun_damage = minetest.get_content_id("syntheticsun:corona_damage")
    for x = minp.x, maxp.x do
        for y = maxp.y, minp.y, -1 do
            for z = minp.z, maxp.z do
                local vi = area:index(x, y, z)
                local below = area:index(x, y-1, z)
                if (data[vi] == c_air or
                    data[vi] == c_sun or
                    data[vi] == c_sun_mid or
                    data[vi] == c_sun_low) and
                    data[below] == c_air and
                    math.abs(pos.x - x) < 3 and
                    math.abs(pos.y - y) < 3 and
                    math.abs(pos.z - z) < 3 then
                       data[vi] = c_sun_damage
                elseif (data[vi] == c_air or
                    data[vi] == c_sun_mid or
                    data[vi] == c_sun_low) and
                    data[below] == c_air and
                    math.abs(pos.x - x) < 7 and
                    math.abs(pos.y - y) < 7 and
                    math.abs(pos.z - z) < 7 then
                    data[vi] = c_sun
                elseif (data[vi] == c_air or
                    data[vi] == c_sun_low) and
                    data[below] == c_air and
                    math.abs(pos.x - x) < 15 and
                    math.abs(pos.y - y) < 15 and
                    math.abs(pos.z - z) < 15 then
                    data[vi] = c_sun_mid
                elseif data[vi] == c_air and
                    data[below] == c_air then
                    data[vi] = c_sun_low
                end
            end
        end
    end
    manip:set_data(data)
    manip:write_to_map()
    manip:update_map()
end

syntheticsun.construct_redgiant = function(min, max, pos)
    local manip = minetest.get_voxel_manip()
    local minp = {x=min.x, y=min.y, z=min.z}
    local maxp = {x=max.x, y=max.y, z=max.z}
    local emin, emax = manip:read_from_map(minp, maxp)
    local area = VoxelArea:new({MinEdge=emin, MaxEdge=emax})
    local data = manip:get_data()
    local c_air = minetest.get_content_id("air")
    local c_sun = minetest.get_content_id("syntheticsun:corona")
    local c_sun_mid = minetest.get_content_id("syntheticsun:corona_mid")
    local c_sun_low = minetest.get_content_id("syntheticsun:corona_low")
    local c_sun_damage = minetest.get_content_id("syntheticsun:corona_damage_heavy")
    for x = minp.x, maxp.x do
        for y = maxp.y, minp.y, -1 do
            for z = minp.z, maxp.z do
                local vi = area:index(x, y, z)
                local below = area:index(x, y-1, z)
                if (data[vi] == c_air or
                    data[vi] == c_sun or
                    data[vi] == c_sun_mid or
                    data[vi] == c_sun_low) and
                    data[below] == c_air and
                    math.abs(pos.x - x) < 6 and
                    math.abs(pos.y - y) < 6 and
                    math.abs(pos.z - z) < 6 then
                       data[vi] = c_sun_damage
                elseif (data[vi] == c_air or
                    data[vi] == c_sun_mid or
                    data[vi] == c_sun_low) and
                    data[below] == c_air and
                    math.abs(pos.x - x) < 16 and
                    math.abs(pos.y - y) < 16 and
                    math.abs(pos.z - z) < 16 then
                    data[vi] = c_sun
                elseif (data[vi] == c_air or
                    data[vi] == c_sun_low) and
                    data[below] == c_air and
                    math.abs(pos.x - x) < 30 and
                    math.abs(pos.y - y) < 30 and
                    math.abs(pos.z - z) < 30 then
                    data[vi] = c_sun_mid
                elseif data[vi] == c_air and
                    data[below] == c_air then
                    data[vi] = c_sun_low
                end
            end
        end
    end
    manip:set_data(data)
    manip:write_to_map()
    manip:update_map()
end

syntheticsun.reconstruct = function(pos, area)
    if string.find(area.data, "whitedwarf") ~= nil then
        syntheticsun.reconstruct_whitedwarf(pos, area)
    elseif string.find(area.data, "redgiant") ~= nil then
        syntheticsun.reconstruct_redgiant(pos, area)
    else
        return
    end
end

syntheticsun.reconstruct_whitedwarf = function(pos, area)
    local sunpos = {
        x=math.abs(area.min.x - area.max.x)/2 + area.min.x,
        y=math.abs(area.min.y - area.max.y)/2 + area.min.y,
        z=math.abs(area.min.z - area.max.z)/2 + area.min.z}
    local below = {x=pos.x, y=pos.y-1, z=pos.z}
    if math.abs(sunpos.x - pos.x) < 3 and
       math.abs(sunpos.y - pos.y) < 3 and
       math.abs(sunpos.z - pos.z) < 3 and
       minetest.get_node(below).name == "air" then
        minetest.set_node(pos, {name="syntheticsun:corona_damage"})
    elseif math.abs(sunpos.x - pos.x) < 7 and
        math.abs(sunpos.y - pos.y) < 7 and
        math.abs(sunpos.z - pos.z) < 7 and
        minetest.get_node(below).name == "air" then
        minetest.set_node(pos, {name="syntheticsun:corona"})
    elseif math.abs(sunpos.x - pos.x) < 15 and
        math.abs(sunpos.y - pos.y) < 15 and
        math.abs(sunpos.z - pos.z) < 15 and
        minetest.get_node(below).name == "air" then
        minetest.set_node(pos, {name="syntheticsun:corona_mid"})
    elseif minetest.get_node(below).name == "air" then
        minetest.set_node(pos, {name="syntheticsun:corona_low"})
    else
        return
    end
end

syntheticsun.reconstruct_redgiant = function(pos, area)
    local sunpos = {
        x=math.abs(area.min.x - area.max.x)/2 + area.min.x,
        y=math.abs(area.min.y - area.max.y)/2 + area.min.y,
        z=math.abs(area.min.z - area.max.z)/2 + area.min.z}
    local below = {x=pos.x, y=pos.y-1, z=pos.z}
    if math.abs(sunpos.x - pos.x) < 6 and
       math.abs(sunpos.y - pos.y) < 6 and
       math.abs(sunpos.z - pos.z) < 6 and
        minetest.get_node(below).name == "air" then
        minetest.set_node(pos, {name="syntheticsun:corona_damage_heavy"})
    elseif math.abs(sunpos.x - pos.x) < 16 and
        math.abs(sunpos.y - pos.y) < 16 and
        math.abs(sunpos.z - pos.z) < 16 and
        minetest.get_node(below).name == "air" then
        minetest.set_node(pos, {name="syntheticsun:corona"})
    elseif math.abs(sunpos.x - pos.x) < 30 and
        math.abs(sunpos.y - pos.y) < 30 and
        math.abs(sunpos.z - pos.z) < 30 and
        minetest.get_node(below).name == "air" then
        minetest.set_node(pos, {name="syntheticsun:corona_mid"})
    elseif minetest.get_node(below).name == "air" then
        minetest.set_node(pos, {name="syntheticsun:corona_low"})
    else
        return
    end
end

syntheticsun.save_data = function()
    local data = minetest.serialize(syntheticsun.suns);
    local path = minetest.get_worldpath().."/mod_syntheticsun.data";

    local file = io.open( path, "w" );
    if( file ) then
        file:write( data );
        file:close();
    else
        print("[Mod syntheticsun] Error: Savefile '"..tostring( path ).."' could not be written.");
    end
end

syntheticsun.restore_data = function()
    --areas = AreaStore('suns')
    local path = minetest.get_worldpath().."/mod_syntheticsun.data";
    local file = io.open( path, "r" );
    if(file) then
        local data = file:read("*all");
        syntheticsun.suns = minetest.deserialize(data);
        file:close();
        local ndx = 1
        for k, v in pairs(syntheticsun.suns) do
            local aid = areas:insert_area(v["edge1"], v["edge2"], v['sun'])
            v["aid"] = aid
            if (k > ndx) then
                ndx = k
            end
        end
        syntheticsun.count = ndx + 1
        syntheticsun.save_data()
    else
        print("[Mod syntheticsun] Error: Read file '"..tostring( path ).."': not found.");
    end
end

syntheticsun.restore_data()

