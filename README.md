mod_syntheticsun
================

This Minetest mod adds two sorts of light sources:

  1. A small star called white dwarf emitting light in a radius of 20
    * Radius 2: damage zone (Player takes 2 hitpoints every second)
    * Radius 6: growing zone
    * Radius 14: medium light
    * Radius 20: low light
  
  2. A bigger star called red giant emitting light in a radius of 50
    * Radius 5: damage zone (Player takes 4 hitpoints every second)
    * Radius 15: growing zone
    * Radius 29: medium light
    * Radius 50: low light

Depends on:

 * homedecor (for power_crystal)

Reciepes
--------
###White Dwarf:

|               |               |                |
|---------------|---------------|----------------| 
|obsidian_glass | mese_block    | obsidian_glass |
|power_crystal  | bucket_water  | power_crystal  |
|obsidian_glass | power_crystal | obsidian_glass |

###Red Giant:

|             |             |              |
|-------------|-------------|--------------|
|             | white_dwarf |              |
| white_dwarf | bucket_lava |  white_dwarf |
|             | white_dwarf |              |